package com.example.matrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.matrimony.R;
import com.example.matrimony.activity.FavoriteCandidateListActivity;
import com.example.matrimony.activity.SearchCandidateActivity;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Utils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList = new ArrayList<>( );
    OnClickListener onClickListner;

    public UserListAdapter(FavoriteCandidateListActivity favoriteCandidateListActivity, ArrayList<UserModel> userList, OnClickListener onClickListener) {

    }

    public UserListAdapter(SearchCandidateActivity searchCandidateActivity, ArrayList<UserModel> tempUserList, OnClickListener onClickListener) {

    }


    public interface OnClickListener {
        void onDeleteButtonClick(int position);

        void onItemClick(int position);

        void onFavoriteButton(int position);
    }


    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getUserName( ) + " " + userList.get(position).getUserSurname( ));
        holder.tvLanguageCity.setText(userList.get(position).getLanguage( ) + " | " + userList.get(position).getCity( ));
        /*holder.tvDOB.setText(userList.get(position).getUserDob());*/

        String dateParts[] = userList.get(position).getUserDob( ).split("/");
        String age = Utils.getAge(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[0]));

        holder.tvAge.setText("Age : " + age);
        userList.get(position).setAge(age);
        holder.ivFavoriteUser.setImageResource(userList.get(position).getIsFavorite( ) == 0 ? R.drawable.ic_favorite : R.drawable.ic_favorite_selected);

        holder.ivDeleteUser.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                if ( onClickListner != null ) {
                    onClickListner.onDeleteButtonClick(position);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                if ( onClickListner != null ) {
                    onClickListner.onItemClick(position);
                }
            }
        });

        holder.ivFavoriteUser.setOnClickListener(new View.OnClickListener( ) {
            @Override
            public void onClick(View v) {
                if ( onClickListner != null ) {
                    onClickListner.onFavoriteButton(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size( );
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvLanguage_City)
        TextView tvLanguageCity;
        /*@BindView(R.id.tvDOB)
        TextView tvDOB;*/
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;
        @BindView(R.id.tvAge)
        TextView tvAge;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
